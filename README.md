Small spring-boot demo project to prove that the information in
http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html 
is correct and that one can have both an external `application.yml` and an
internal `application.yml` at the same time.

## Usage

Run the application: `mvn package && java -jar target/demo-1.0.jar`

```
2016-11-19 15:30:55.503  INFO 3624 --- [           main] c.e.SpringBootMultiConfigDemoApplication : common property was read from external application.yml
2016-11-19 15:30:55.503  INFO 3624 --- [           main] c.e.SpringBootMultiConfigDemoApplication : internal property was read from internal application.yml
2016-11-19 15:30:55.503  INFO 3624 --- [           main] c.e.SpringBootMultiConfigDemoApplication : external property was read from external application.yml
```

Point proven!