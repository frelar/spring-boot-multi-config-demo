package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SpringBootMultiConfigDemoApplication {
    private static final Logger logger = LoggerFactory.getLogger(SpringBootMultiConfigDemoApplication.class);
    @Value("${common.property}")
    private String common;
    @Value("${internal.property}")
    private String internal;
    @Value("${external.property}")
    private String external;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMultiConfigDemoApplication.class, args);
    }

    @PostConstruct
    public void writePropertiesToLog() {
        logger.info("common property " + common);
        logger.info("internal property " + internal);
        logger.info("external property " + external);
    }
}
    